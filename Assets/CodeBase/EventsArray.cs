﻿using UnityEngine;
using UnityEngine.Events;

namespace CodeBase
{
    public class EventsArray : MonoBehaviour
    {
        public UnityEvent[] Events;
        public void StartEvent(int index) => 
            Events[index]?.Invoke();
    }
}