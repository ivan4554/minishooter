﻿using System;
using UnityEngine;

namespace CodeBase
{
    public class Follow : MonoBehaviour
    {
        public Transform Target;
        public float LerpRate;

        private void Update()
        {
            if (Target)
                transform.position = Vector3.Lerp(transform.position, Target.position, Time.deltaTime * LerpRate);
        }
    }
}