﻿using System.Collections;
using UnityEngine;

namespace CodeBase
{
    public class Blink : MonoBehaviour
    {
        private const string EmissionName = "_EmissionColor";
        
        public Renderer[] Renderers;
        
        public void StartBlink() => 
            StartCoroutine(BlinkEffect());

        private IEnumerator BlinkEffect()
        {
            float colorChannel = 1;
            while (colorChannel >= 0)
            {
                colorChannel -= Time.deltaTime;
                
                foreach (Renderer renderer in Renderers)
                    for (int i = 0; i < renderer.materials.Length; i++)
                        renderer.materials[i].SetColor(EmissionName, new Color(Mathf.Sin(colorChannel), 0, 0, 1));

                yield return null;
            }
        }
    }
}