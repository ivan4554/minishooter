﻿using System;
using UnityEngine;

namespace CodeBase.Rope
{
    public class Hook : MonoBehaviour
    {
        [Header("Ignore collision")] 
        public Collider MyCollider;
        public Collider PlayerCollider;

        public RopeGun RopeGun;
        public Rigidbody Rigidbody;
        
        private FixedJoint _fixedJoint;

        private void Start() => 
            Physics.IgnoreCollision(MyCollider,PlayerCollider);

        private void OnCollisionEnter(Collision other)
        {
            if(!PlayerCollider || !RopeGun)
                return;
            
            if (!_fixedJoint && other.rigidbody)
            {
                _fixedJoint = gameObject.AddComponent<FixedJoint>();
                _fixedJoint.connectedBody = other.rigidbody;
                RopeGun.CreateSpring();
            }
        }
        public void StopFix()
        {
            if(_fixedJoint)
                Destroy(_fixedJoint);
        }
    }
}