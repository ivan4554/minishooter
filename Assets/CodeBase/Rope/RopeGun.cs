﻿using System;
using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.Rope
{
    public enum RopeState
    {
        Disabled,
        Fly,
        Active
    }

    public class RopeGun : MonoBehaviour
    {
        public RopeState CurrentRopeState;

        public PlayerMove PlayerMove; 
        public Hook Hook;
        public RopeRenderer RopeRenderer;
        
        public Transform Spawn;
        public Transform RopeStart;
        
        public float SpeedHook = 15f;
        
        [Header("Spring Installation")]
        public int SpringForce = 100;
        public int SpringDamper = 5;
        
        private SpringJoint _springJoint;
        private float _length;
        private bool _isJump;

        private void Update()
        {
            if (Input.GetMouseButtonDown(2)) 
                Shot();

            CheckDistance();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (CurrentRopeState == RopeState.Active && !PlayerMove.IsGround)
                    _isJump = true;
                
                DestroyRope();
            }


            if (CurrentRopeState != RopeState.Disabled)
                RopeRenderer.Draw(RopeStart.position, Hook.transform.position, _length);
        }

        private void FixedUpdate()
        {
            if (_isJump)
            {
                PlayerMove.Jump();
                _isJump = false;
            }
        }

        public void CreateSpring()
        {
            if (_springJoint) return;

            
            _springJoint = gameObject.AddComponent<SpringJoint>();
            SpringInstallation();
            
            CurrentRopeState = RopeState.Active;
        }

        private void CheckDistance()
        {
            if (CurrentRopeState != RopeState.Fly) 
                return;

            if (SetMaxDistance() > 20)
            {
                RopeRenderer.Hide();
                Hook.gameObject.SetActive(false);
                CurrentRopeState = RopeState.Disabled;
            }

        }

        private void DestroyRope()
        {
            DestroySpring();
            Hook.gameObject.SetActive(false);
            RopeRenderer.Hide();
            
            CurrentRopeState = RopeState.Disabled;
        }

        private void Shot()
        {
            _length = 1f;
            
            DestroySpring();
            Hook.StopFix();
            Hook.gameObject.SetActive(true);
            HookInstallation();
        }

        private void SpringInstallation()
        {
            _springJoint.connectedBody = Hook.Rigidbody;
            _springJoint.anchor = RopeStart.localPosition;
            
            _springJoint.autoConfigureConnectedAnchor = false;
            _springJoint.connectedAnchor = Vector3.zero;
            
            _springJoint.spring = SpringForce;
            _springJoint.damper = SpringDamper;
            
            _springJoint.maxDistance = SetMaxDistance();
        }

        private void DestroySpring()
        {
            if(_springJoint)
                Destroy(_springJoint);
        }

        private void HookInstallation()
        {
            Hook.transform.position = Spawn.position;
            Hook.transform.rotation = Spawn.rotation;
            Hook.Rigidbody.velocity = SpeedHook * Spawn.forward;

            CurrentRopeState = RopeState.Fly;
        }

        private float SetMaxDistance() => 
            _length = Vector3.Distance(RopeStart.position, Hook.transform.position);
    }
}