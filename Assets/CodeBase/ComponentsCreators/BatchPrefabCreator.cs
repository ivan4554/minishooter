﻿using UnityEngine;

namespace CodeBase.ComponentsCreators
{
    public class BatchPrefabCreator : MonoBehaviour
    {
        public GameObject Prefab;
        
        public Transform[] Points;
        
        public void Create()
        {
            foreach (Transform point in Points) 
                Instantiate(Prefab, point.position, point.rotation);
        }
        
    }
}