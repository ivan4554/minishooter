﻿using System;
using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.Loot
{
    public class LootHeal : MonoBehaviour
    {
        public int AddHealthValue;

        private void OnTriggerEnter(Collider other)
        {
            PlayerHealth playerHealth = other.attachedRigidbody.GetComponent<PlayerHealth>();

            if (playerHealth)
            {
                playerHealth.AddHealth(AddHealthValue);
                Destroy(gameObject);
            }
        }
    }
}