﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace CodeBase
{
    public class Walker : MonoBehaviour
    {
        [Header("Targets")]
        public Transform LeftTarget;
        public Transform RightTarget;

        [Header("Values")]
        public float Speed;
        public float StopTime;

        [Header("Events")] 
        public UnityEvent EventOnLeftTarget;
        public UnityEvent EventOnRightTarget;

        public Transform RayStart;
        
        public Direction CurrentTarget;

        private bool _isStopped;

        private void Start()
        {
            LeftTarget.parent = null;
            RightTarget.parent = null;
        }

        private void Update()
        {
            if (_isStopped)
                return;

            ChangeDirection();
        }

        private void ChangeDirection()
        {
            switch (CurrentTarget)
            {
                case Direction.Left:
                    transform.position -= new Vector3(Time.deltaTime * Speed, 0, 0);
                    
                    if (transform.position.x < LeftTarget.transform.position.x)
                    {
                        CurrentTarget = Direction.Right;
                        _isStopped = true;
                        Invoke(nameof(ContinueWalk),StopTime);
                        EventOnLeftTarget.Invoke();
                    }
                    break;
                case Direction.Right:
                    transform.position += new Vector3(Time.deltaTime * Speed, 0, 0);
                    
                    if (transform.position.x > RightTarget.transform.position.x)
                    {
                        CurrentTarget = Direction.Left;
                        _isStopped = true;
                        Invoke(nameof(ContinueWalk),StopTime);
                        EventOnRightTarget.Invoke();
                    }
                    break;
            }

            RaycastHit hit;
            if (Physics.Raycast(RayStart.position, Vector3.down, out hit)) 
                transform.position = hit.point;
        }

        private void ContinueWalk() => 
            _isStopped = false;
    }
}