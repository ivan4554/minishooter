﻿using CodeBase.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace CodeBase.PlayerBase
{
    public class PlayerHealth : MonoBehaviour, IHealth
    {
        public int MaxHealth;

        public HealthUI HealthUI;
        public AudioSource AddHealthSound;

        public UnityEvent EventOnTakeDamage;
        public UnityEvent EventOnDie;
        
        private int _health;

        private void Start()
        {
            _health = MaxHealth;
            
            HealthUI.Setup(_health);
        }

        public void TakeDamage(int damage)
        {
            _health -= damage;
            
            if (damage > 1 && _health < 0) 
                HealthUI.RemoveHeart();
            else
                for (int i = 0; i < damage; i++)
                    HealthUI.RemoveHeart();

            EventOnTakeDamage.Invoke();
            if (_health <= 0) 
                Die();
        }

        public void AddHealth(int healthValue)
        {
            _health += healthValue;

            AddHealthSound.Play();
            
            if (_health > MaxHealth)
                _health = MaxHealth;
            else
                HealthUI.AddHeart();
        }

        private void Die()
        {
            Debug.Log($"You lose");
            Destroy(gameObject);
            EventOnDie?.Invoke();
        }
    }
}