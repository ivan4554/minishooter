﻿using CodeBase.Guns;
using UnityEngine;

namespace CodeBase.PlayerBase
{
    public class PlayerArmory : MonoBehaviour
    {
        public Gun[] Guns;
        public int CurrentGunIndex;

        public AudioSource AddBulletsSound;
        private void Start() => 
            TakeGunByIndex(CurrentGunIndex);

        public void TakeGunByIndex(int gunIndex)
        {
            for (int i = 0; i < Guns.Length; i++) 
                Guns[i].gameObject.SetActive(gunIndex == i);
        }

        public void AddBullets(int gunIndex, int numberOfBullets)
        {
            for (int i = 0; i < Guns.Length; i++)
            {
                if (i == gunIndex)
                {
                    TakeGunByIndex(gunIndex);
                    Guns[i].AddBullets(numberOfBullets);
                    AddBulletsSound.Play();
                }
            }
        }
    }
}