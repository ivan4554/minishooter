﻿using System;
using UnityEngine;

namespace CodeBase.PlayerBase
{
    public class PlayerMove : MonoBehaviour
    {
        [Header("Components")]
        public Transform ColliderTransform;
        public Rigidbody Rigidbody;

        [Header("Hat")]
        public Transform HatTransform;
        public float SmoothTurn;

        [Header("Jump and Speed")]
        public float ForceSpeed = 2.5f;
        public float JumpForce;
        public float TurnJump = 20;
        public float MaxSpeed = 5f;

        public float Friction;
        public float SmoothSquat;

        public bool IsGround;

        private bool _isActiveJump;
        private int _jumpFrameCounter;

        private void Start() => 
            Rigidbody.maxAngularVelocity = 50;

        private void Update()
        {
            Crouch();

            if (Input.GetKey(KeyCode.LeftShift))
            {
                ForceSpeed = 3f;
                MaxSpeed = 7f;
                Friction = 0.3f;
            }

            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                ForceSpeed = 2.5f;
                MaxSpeed = 5f;
                Friction = 0.6f;
            }

            if (Input.GetKeyDown(KeyCode.Space) && IsGround)
            {
                _isActiveJump = true;
                _jumpFrameCounter = 0;
            }
        }

        private void FixedUpdate()
        {
            float speedMultiplier = 1;

            if (!IsGround)
            {
                speedMultiplier = 0.1f;

                if (Rigidbody.velocity.x > MaxSpeed && IsPositiveHorizontal())
                    speedMultiplier = 0;

                if (Rigidbody.velocity.x < -MaxSpeed && !IsPositiveHorizontal())
                    speedMultiplier = 0;
            }

            Rigidbody.AddForce(Input.GetAxis("Horizontal") * ForceSpeed * speedMultiplier, 0,0,ForceMode.VelocityChange);

            if (IsGround)
            {
                Rigidbody.AddForce(-Rigidbody.velocity.x * Friction, 0, 0, ForceMode.VelocityChange);
                
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime);
            }


            if (_isActiveJump)
            {
                Jump();
                _isActiveJump = false;
            }

            _jumpFrameCounter += 1;
            
             if (_jumpFrameCounter == 2)
             {
                 Rigidbody.freezeRotation = false;
                 Rigidbody.AddRelativeTorque(0,0,TurnJump, ForceMode.VelocityChange);
             }
        }

        public void Jump() => 
            Rigidbody.AddForce(0, JumpForce, 0, ForceMode.VelocityChange);

        private void OnCollisionStay(Collision other)
        {
            Vector3 normal = other.contacts[0].normal;
            float dot = Vector3.Dot(normal, Vector3.up);
            if (dot > Mathf.Abs(0.5f))
            {
                IsGround = true;
                Rigidbody.freezeRotation = true;
            }
        }

        private void OnCollisionExit(Collision other) => 
            IsGround = false;

        private void Crouch()
        {
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.S) || !IsGround)
            {
                TurningHat(IndicateTurnHat());
                LerpSquat(scaleY: 0.5f);
            }
            else
            {
                TurningHat(Quaternion.identity);
                LerpSquat(scaleY: 1);
            }
        }

        private void TurningHat(Quaternion turnAngles)
        {
            HatTransform.rotation = Quaternion.Lerp(
                HatTransform.rotation,
                turnAngles,
                Time.deltaTime * SmoothTurn);
        }

        private static Quaternion IndicateTurnHat()
        {
            Vector3 angles = new Vector3(-10.6f, 0, 0);
            Quaternion turnAngles = Quaternion.Euler(angles);
            return turnAngles;
        }

        private void LerpSquat(float scaleY)
        {
            ColliderTransform.localScale = Vector3.Lerp(
                ColliderTransform.localScale,
                new Vector3(1, scaleY, 1),
                Time.deltaTime * SmoothSquat);
        }

        private static bool IsPositiveHorizontal() =>
            Input.GetAxis("Horizontal") > 0;
    }
}
