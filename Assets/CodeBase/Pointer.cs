﻿using System;
using UnityEngine;

namespace CodeBase
{
    public class Pointer : MonoBehaviour
    {
        public Transform Aim;
        public Transform Body;
        public Camera PlayerCamera;
        
        public float SmoothTurn;
        private void LateUpdate()
        {
            Ray ray = PlayerCamera.ScreenPointToRay(Input.mousePosition);
            
            Debug.DrawRay(ray.origin,ray.direction * 50,Color.red);
            var plane = CreatePlane();

            if (plane.Raycast(ray, out float distance))
            {
                Vector3 point = ray.GetPoint(distance);
                Aim.position = point;

                TurnTransform();
            }
        }

        private void TurnTransform()
        {
            Vector3 atVectorTo = Aim.position - transform.position;
            transform.rotation = Quaternion.LookRotation(atVectorTo);
            
            TurningBody();
        }

        private void TurningBody()
        {
            Quaternion turnBody = new Quaternion(0, Mathf.Clamp(-transform.rotation.y,-0.2f,0.2f), 0, transform.rotation.w);
            Body.rotation = Quaternion.Lerp(Body.rotation, turnBody, Time.deltaTime * SmoothTurn);
        }

        private static Plane CreatePlane() => 
            new Plane(-Vector3.forward, Vector3.zero);
    }
}