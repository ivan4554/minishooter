﻿using System;
using UnityEngine;

namespace CodeBase
{
    public class Bullet : MonoBehaviour
    {
        public GameObject EffectBulletPrefab;

        private void Start() => 
            Destroy(gameObject,5);

        private void OnCollisionEnter(Collision other) => 
            DestroyBullet();

        public void DestroyBullet()
        {
            CreateEffect();
            Destroy(gameObject);
        }

        private void CreateEffect() => 
            Instantiate(EffectBulletPrefab, transform.position, Quaternion.identity);
    }
}