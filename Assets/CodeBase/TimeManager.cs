﻿using System;
using UnityEngine;

namespace CodeBase
{
    public class TimeManager : MonoBehaviour
    {
        public float TimeScale = 0.2f;
        private float _timeFixedDeltaTime;

        private void Start() => 
            _timeFixedDeltaTime = Time.fixedDeltaTime;

        private void Update()
        {
            Time.timeScale = Input.GetMouseButton(1) ? TimeScale : 1f;

            Time.fixedDeltaTime = _timeFixedDeltaTime * Time.timeScale;
        }

        private void OnDestroy() => 
            Time.fixedDeltaTime = _timeFixedDeltaTime;
    }
}