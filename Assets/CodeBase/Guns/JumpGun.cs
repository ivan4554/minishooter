﻿using System;
using CodeBase.UI;
using UnityEngine;

namespace CodeBase.Guns
{
    public class JumpGun : MonoBehaviour
    {
        public Rigidbody PlayerRigidbody;
        public ChargeIcon ChargeIcon;
        
        public float Speed;
        public float MaxCharge;

        private float _currentCharge;
        private bool _isClicked;
        private bool _isCharged;
        private void Update()
        {
            _currentCharge += Time.unscaledDeltaTime;

            ChargeIcon.SetValueCharge(_currentCharge,MaxCharge);
            
            if (_currentCharge >= MaxCharge)
            {
                _isCharged = true;
                ChargeIcon.StopCharge();
            }

            if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButtonDown(0) && _isCharged)
            {
                _isClicked = true;
                _isCharged = false;
                _currentCharge = 0;
                ChargeIcon.StartCharge();
            }
        }

        private void FixedUpdate()
        {
            if (_isClicked)
            {
                PlayerRigidbody.AddForce(-transform.forward * Speed,ForceMode.VelocityChange);
                _isClicked = false;
            }
        }
    }
}