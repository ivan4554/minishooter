﻿using System;
using CodeBase.PlayerBase;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.Guns
{
    public class Automat : Gun
    {
        [Header("Automat")]
        public int DefaultNumberOfBullets;
        public Text NumberOfBulletsText;
        public PlayerArmory PlayerArmory;
        
        private int _remainingNumberOfBullets;

        private void Start()
        {
            PanelNumberOfBullets(true);
            _remainingNumberOfBullets = DefaultNumberOfBullets;
            RefreshText();
        }

        protected override void Shot()
        {
            _remainingNumberOfBullets--;
            RefreshText();
            base.Shot();

            if (_remainingNumberOfBullets == 0)
            {
                PlayerArmory.TakeGunByIndex(0);
                PanelNumberOfBullets(false);
            }
        }

        public override void AddBullets(int numberOfBullets)
        {
            _remainingNumberOfBullets += numberOfBullets;
            RefreshText();
        }

        private void RefreshText() => 
            NumberOfBulletsText.text = _remainingNumberOfBullets.ToString();

        private void PanelNumberOfBullets(bool isActivePanel) => 
            NumberOfBulletsText.transform.parent.gameObject.SetActive(isActivePanel);
    }
}