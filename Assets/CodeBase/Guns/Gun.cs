﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace CodeBase.Guns
{
    public class Gun : MonoBehaviour
    {
        public GameObject PrefabBullet;
        public Transform Spawn;
        
        public GameObject Flash;
        public AudioSource ShotSound;
        public ParticleSystem SmokeEffectSystem;
        
        public float SpeedBullet;
        public float ShotDelay;

        private float _timer;


        private void Update()
        {
            _timer += Time.unscaledDeltaTime;

            if (Input.GetMouseButton(0) && _timer > ShotDelay) 
                Shot();
        }

        protected virtual void Shot()
        {
            _timer = 0;
            CreateBullet();
            PlayingSound();

            Flash.SetActive(true);
            SmokeEffectSystem.Play();
            Invoke(nameof(HideFlash), 0.08f);
        }

        private void PlayingSound()
        {
            ShotSound.pitch = Random.Range(0.7f, 1.5f);
            ShotSound.Play();
        }

        private void HideFlash() => 
            Flash.SetActive(false);

        private void CreateBullet()
        {
            GameObject newBullet = Instantiate(PrefabBullet, Spawn.position, Quaternion.identity);
            newBullet.GetComponent<Rigidbody>().velocity = Spawn.forward * SpeedBullet;
        }

        public virtual void AddBullets(int numberOfBullets)
        {
        }
    }
}