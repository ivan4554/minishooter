﻿using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.UI
{
    public class ChargeIcon : MonoBehaviour
    {
        public Image Background;
        public Image Foreground;
        public Text ChargeText;

        public void StartCharge()
        {
            Background.color = new Color(1,1,1,0.2f);
            Foreground.enabled = true;
            ChargeText.enabled = true;
        }

        public void StopCharge()
        {
            Background.color = new Color(1,1,1,1f);
            Foreground.enabled = false;
            ChargeText.enabled = false;
        }


        public void SetValueCharge(float currentCharge,float maxCharge)
        {
            Foreground.fillAmount = currentCharge / maxCharge;
            ChargeText.text = Mathf.Ceil(maxCharge - currentCharge).ToString();
        }
    }
}