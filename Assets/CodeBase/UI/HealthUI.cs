﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.UI
{
    public class HealthUI : MonoBehaviour
    {
        private const string PathHeart = "Heart";
        
        private readonly List<GameObject> _hearts = new List<GameObject>();
        
        public void Setup(int health)
        {
            for (int i = 0; i < health; i++)
                AddHeart();
        }

        public void AddHeart() => 
            _hearts.Add(InstantiateHeart());

        public void RemoveHeart()
        {
            Destroy(_hearts[0]);
            _hearts.RemoveAt(0);
        }

        private GameObject InstantiateHeart() => 
            Instantiate(Resources.Load<GameObject>(PathHeart), transform);
    }
}