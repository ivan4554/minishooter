﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CodeBase.UI
{
    public class MenuWindow : MonoBehaviour
    {
        public MonoBehaviour[] UnavailablesObjects;
        public AudioSource Music;
        private void OnEnable()
        {
            Debug.Log("Enable");
            CheckProbablyObjects(false);
            Time.timeScale = 0.01f;
        }

        private void OnDisable()
        {
            Debug.Log("Disable");
            CheckProbablyObjects(true);
            Time.timeScale = 1f;
        }

        public void StartOver() => 
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        public void SetVolume(float volume) => 
            AudioListener.volume = volume;

        public void SetMusicEnabled(bool isActive) => 
            Music.enabled = isActive;

        private void CheckProbablyObjects(bool isEnable)
        {
            foreach (MonoBehaviour unavailablesObject in UnavailablesObjects)
                unavailablesObject.enabled = isEnable;
        }
    }
}