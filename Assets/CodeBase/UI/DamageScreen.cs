﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.UI
{
    public class DamageScreen : MonoBehaviour
    {
        public Image DamageScreenImage;
        private void Start() => 
            DamageScreenImage = GetComponent<Image>();

        public void StartDamageEffect() => 
            StartCoroutine(EffectCoroutine());

        private IEnumerator EffectCoroutine()
        {
            DamageScreenImage.enabled = true;
            
            float alphaChannel = 1;
            
            while (alphaChannel >= 0)
            {
                alphaChannel -= Time.deltaTime;
                DamageScreenImage.color = new Color(1,0,0,alphaChannel);
                yield return null;
            }
            DamageScreenImage.enabled = false;
        }
    }
}