﻿namespace CodeBase
{
    public interface IHealth
    {
        void TakeDamage(int damage);
    }
}