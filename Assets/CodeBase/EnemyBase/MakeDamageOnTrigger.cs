﻿using System;
using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.EnemyBase
{
    public class MakeDamageOnTrigger : MonoBehaviour
    {
        public int Damage;
        private void OnTriggerEnter(Collider other)
        {
            if (!other.attachedRigidbody)
                return;
            
            PlayerHealth playerHealth = other.attachedRigidbody.GetComponent<PlayerHealth>();
            if (playerHealth) 
                    playerHealth.TakeDamage(Damage);
        }
    }
}