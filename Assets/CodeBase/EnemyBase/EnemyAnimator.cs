﻿using System;
using UnityEngine;

namespace CodeBase.EnemyBase
{
  public class EnemyAnimator : MonoBehaviour
  {
    private static readonly int Attack = Animator.StringToHash("Attack");
    private static readonly int Hit = Animator.StringToHash("Hit");

    private Animator _animator;
    private void Awake() => 
      _animator = GetComponent<Animator>();

    public void PlayHit() => _animator.SetTrigger(Hit);
    public void PlayAttack() => _animator.SetTrigger(Attack);
  }
}