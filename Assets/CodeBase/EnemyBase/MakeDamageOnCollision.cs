﻿using System;
using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.EnemyBase
{
    public class MakeDamageOnCollision : MonoBehaviour
    {
        public int Damage;

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.GetComponent<PlayerHealth>())
            {
                PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
                playerHealth.TakeDamage(Damage);
            }
        }
    }
}