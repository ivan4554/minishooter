﻿using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.EnemyBase.Enemies
{
    public class Carrot : MonoBehaviour
    {
        public Rigidbody Rigidbody;
        public float Speed;

        private void Start()
        {
            if(!FindObjectOfType<PlayerHealth>())
                return;
            
            transform.rotation = Quaternion.identity;
            
            Transform transformPlayer = FindObjectOfType<PlayerHealth>().transform;
            
            Vector3 toPlayer = (transformPlayer.position - transform.position).normalized;

            Rigidbody.velocity = toPlayer * Speed;
            
            Destroy(gameObject,15);
        }
    }
}