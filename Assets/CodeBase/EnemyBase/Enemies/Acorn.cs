﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace CodeBase.EnemyBase.Enemies
{
    public class Acorn : MonoBehaviour
    {
        public Rigidbody Rigidbody;
        public Vector3 Force;
        public float RotationSpeed;

        private void Start()
        {
            Rigidbody.AddRelativeForce(Force, ForceMode.VelocityChange);
            Rigidbody.angularVelocity = new Vector3(
                Random.Range(-RotationSpeed,RotationSpeed),
                Random.Range(-RotationSpeed,RotationSpeed),
                Random.Range(-RotationSpeed,RotationSpeed));
        }
    }
}