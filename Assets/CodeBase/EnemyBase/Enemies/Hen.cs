﻿using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.EnemyBase.Enemies
{
    public class Hen : MonoBehaviour
    {
        public Rigidbody Rigidbody;
        public float Speed = 3f;
        public float TimeToReachSpeed = 1f;

        private Transform _playerTransform;

        private void Start() => 
            _playerTransform = FindObjectOfType<PlayerHealth>().transform;

        private void FixedUpdate()
        {
            if (!_playerTransform)
                return;
            
            Vector3 toPlayer = (_playerTransform.position - transform.position).normalized;
            
            Vector3 force = Rigidbody.mass * (toPlayer * Speed - Rigidbody.velocity) / TimeToReachSpeed;
            Rigidbody.AddForce(force);
        }
    }
}