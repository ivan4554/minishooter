﻿using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.EnemyBase.Enemies
{
    public class Rocket : MonoBehaviour
    {
        public float Speed;
        public float RotationSpeed;

        private Transform _playerTransform;
        private void Start()
        {
            if(!FindObjectOfType<PlayerHealth>())
                return;
            _playerTransform = FindObjectOfType<PlayerHealth>().transform;
        }

        private void Update()
        {
            if(!_playerTransform)
                return;
            
            Vector3 toPlayer = _playerTransform.position - transform.position;

            transform.position += Time.deltaTime * Speed * transform.forward;
            Quaternion targetLook = Quaternion.LookRotation(toPlayer,Vector3.forward);

            transform.rotation = Quaternion.Lerp(transform.rotation,targetLook,Time.deltaTime * RotationSpeed);

        }
    }
}