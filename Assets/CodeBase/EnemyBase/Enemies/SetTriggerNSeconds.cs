﻿using UnityEngine;

namespace CodeBase.EnemyBase.Enemies
{
    public class SetTriggerNSeconds : MonoBehaviour
    {

        public EnemyAnimator Animator;
        public float ShotPeriod = 7f;
        
        private float _timer;

        private void Update() => 
            PeriodAttack();

        private void PeriodAttack()
        {
            _timer += Time.deltaTime;

            if (_timer > ShotPeriod)
            {
                _timer = 0;
                Animator.PlayAttack();
            }
        }
    }
}