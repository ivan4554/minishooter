﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace CodeBase.EnemyBase
{
    public class EnemyHealth : MonoBehaviour, IHealth
    {
        public int MaxHealth = 1;

        public UnityEvent EventOnTakeDamage;
        public UnityEvent EventOnDie;
        
        private int _health;

        private void Start() => 
            _health = MaxHealth;

        public void TakeDamage(int damage)
        {
            _health -= damage;
            
            EventOnTakeDamage?.Invoke();
            
            if (_health <= 0) 
                Die();
        }

        private void Die()
        {
            Destroy(gameObject);
            EventOnDie?.Invoke();
        }
    }
}