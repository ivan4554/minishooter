﻿using System;
using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.EnemyBase
{
    public class TakeDamageOnTrigger : MonoBehaviour
    {
        public EnemyHealth EnemyHealth;
        public bool IsDieOnAnyCollision;
        private void OnTriggerEnter(Collider other)
        {
            Bullet bullet = other.GetComponent<Bullet>();

            if (bullet)
            {
                EnemyHealth.TakeDamage(1);
                bullet.DestroyBullet();
            }
            if (IsDieOnAnyCollision && !other.isTrigger) 
                EnemyHealth.TakeDamage(1000);
        }
    }
}