﻿using System;
using UnityEngine;

namespace CodeBase.EnemyBase
{
    [RequireComponent(typeof(EnemyHealth))]
    public class TakeDamageOnCollision : MonoBehaviour
    {
        public EnemyHealth EnemyHealth;

        public bool IsDieOnAnyCollision;
        
        private void OnCollisionEnter(Collision other)
        {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            
            if (bullet) 
                EnemyHealth.TakeDamage(1);

            if (IsDieOnAnyCollision) 
                EnemyHealth.TakeDamage(1000);
        }
    }
}