﻿using CodeBase.PlayerBase;
using UnityEngine;

namespace CodeBase.Rotates
{
    public class RotateToPlayer : MonoBehaviour
    {
        [Header("Повороты влево и вправо")]
        public Vector3 LeftEuler;
        public Vector3 RightEuler;

        public float RotationSpeed = 5f;
        
        private Transform _playerTransform;
        private Vector3 _targetEuler;

        private void Start() => 
            _playerTransform = FindObjectOfType<PlayerHealth>().transform;

        private void Update()
        {
            if(!_playerTransform)
                return;
            
            _targetEuler = transform.position.x < _playerTransform.position.x ? RightEuler : LeftEuler;
            
            transform.rotation = Quaternion.Lerp(transform.rotation,Quaternion.Euler(_targetEuler), Time.deltaTime * RotationSpeed);
        }
    }
}