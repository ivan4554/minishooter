﻿using System;
using UnityEditor;
using UnityEngine;

namespace CodeBase.Observer
{
    public class ActivateByDistance : MonoBehaviour
    {
        public float DistanceToActivate = 20f;

        private Activator _activator; 
        private void Start()
        {
            _activator = FindObjectOfType<Activator>();
            _activator.AddObject(this);
        }

        public void CheckDistanceToPlayer(Transform player)
        {
            float distance = Vector3.Distance(transform.position, player.position); 
            gameObject.SetActive(distance < DistanceToActivate);
        }

        private void OnDestroy() => 
            _activator.RemoveObject(this);

        private void OnDrawGizmosSelected()
        {
            #if UNITY_EDITOR
                Handles.color = Color.grey;
                Handles.DrawWireDisc(transform.position,Vector3.forward, DistanceToActivate);
            #endif
        }
    }
}