﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CodeBase.Observer
{
    public class Activator : MonoBehaviour
    {
        public Transform PlayerTransform;
        
        private readonly List<ActivateByDistance> _objectsToActivate = new List<ActivateByDistance>();


        private void Update()
        {
            if(PlayerTransform)
                foreach (ActivateByDistance objectToActivate in _objectsToActivate)
                    objectToActivate.CheckDistanceToPlayer(PlayerTransform);
        }

        public void AddObject(ActivateByDistance activateObject) => 
            _objectsToActivate.Add(activateObject);
        
        public void RemoveObject(ActivateByDistance activateObject) => 
            _objectsToActivate.Remove(activateObject);
    }
}