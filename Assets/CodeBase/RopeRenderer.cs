﻿using System;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace CodeBase
{
    public class RopeRenderer : MonoBehaviour
    {
        public LineRenderer LineRenderer;
        public int Segments = 10;
        
        private float _interpolant;
        private float _offset;
        private void Start() => 
            LineRenderer.positionCount = Segments + 1;

        public void Draw(Vector3 a, Vector3 b, float length)
        {
            LineRenderer.enabled = true;
            _interpolant = Vector3.Distance(a, b) / length;
            _offset = Mathf.Lerp(length / 2, 0, _interpolant);

            Vector3 ADown = a + Vector3.down * _offset;
            Vector3 BDown= b + Vector3.down * _offset;
            
            for (int i = 0; i < Segments + 1; i++)
                LineRenderer.SetPosition(i,
                    Bezier.GetPoint(a, ADown, BDown, b, (float) i / Segments));
        }

        public void Hide() => LineRenderer.enabled = false;
    }
}